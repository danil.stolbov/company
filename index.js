const yojji = new Company('yojji', 10);

renderCompanyHeader(yojji);

yojji.registerUserCreateCallback(addDataInTable);
yojji.registerUserCreateCallback(() => renderNotification(yojji, 'User was created'));

yojji.registerUserUpdateCallback(changeOrDeleteUserData);
yojji.registerUserUpdateCallback((user) => {
	if (user.id) {
		return;
	}

	renderNotification(yojji, 'User was removed');
});

window.yojji = yojji;