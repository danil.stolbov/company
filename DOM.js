;(function() {
	renderNotification = ({ name }, message) => {
		const templateNotification = document.querySelector('#template__notification');
		const clone = templateNotification.content.firstElementChild.cloneNode(true);
		clone.querySelector('.me-auto').textContent = name;
		clone.querySelector('.toast-body').textContent = message;
		document.body.appendChild(clone);

		setTimeout(() => {
			clone.remove();
		}, 3000);
	}

	const renderCompanyHeader = ({ name, maxSize, curSize }) => {
		const headerTemplate = document.querySelector("#header-template");
		const clone = headerTemplate.content.cloneNode(true);
		clone.querySelector(".logo").textContent = name;
		clone.querySelector(".curSize").textContent = curSize;
		clone.querySelector(".maxSize").textContent = maxSize;
		document.body.appendChild(clone);
	}

	const renderUserData = (selector, { name, lastName, isAdmin, role, id }) => {
		const arr = [];
		arr.push('*', name, lastName, role, id, 'x');

		selector.forEach((item, i) => item.textContent = arr[i]);
	}

	const addDataInTable = (user) => {
		const templateHeader = document.querySelector("#header-template");
		const clone = templateHeader.content.cloneNode(true);
		const btnDelete = clone.querySelector("#btn-delete");
		const rows = clone.querySelectorAll("td");

    renderUserData(rows, user);

		btnDelete.addEventListener('click', () => {
			admin.deleteUser(user.id);
		});

		document.querySelector('tbody').appendChild(clone);
	}

	const changeOrDeleteUserData = (user) => {
		const userId = document.querySelectorAll("#userId");
		const [findElem] = userId.filter(item => item.textContent == user.id);

		if (!user.id) {
			const [findId] = userId.filter(item => item.textContent == user.id);
			findId.parentNode.remove();
			return
		}
		const allParent = findElem.parentNode.parentNode.querySelectorAll('td');
		renderUserData(allParent, user);
	}

	window.renderNotification = renderNotification;
	window.renderCompanyHeader = renderCompanyHeader;
	window.addDataInTable = addDataInTable;
	window.changeOrDeleteUserData = changeOrDeleteUserData;
})();