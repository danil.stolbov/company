;(function() {
  
  const data = {};

  const ERRORS = {
    ALREADY_HAVE_ADMIN: 'Company already have admin',
    INCORRECT_PASSWORD: 'Password incorrect!',
    INCORRECT_TOKEN: 'Token incorrect',
    TOO_LOT_EMPLOYEES: 'You company have to lot employees',
    CANT_DELETE_ADMIN: 'You can\'t delete admin'
  }

  const ADMIN_TOKEN = 'secret_token';
  const ENTER_PASSWORD = 'Enter password';

	class User {
    #token;
    #name;
    #lastName;
    #role
    #password;
    #isAdmin;
    constructor(name, lastName, isAdmin = false, role = 'role') {
      this.#name = name;
      this.#lastName = lastName;
      this.#isAdmin = isAdmin;
      this.#role = role;
      this.id = Math.floor(Math.random() * 10000);

      if (this.#isAdmin) {
        this.token = ADMIN_TOKEN;
        this.#password = prompt(ENTER_PASSWORD);
        this.createUser = function() {
          this.#checkAdmin();
          if (data.user.users.length === data.admin.company.maxSize) {
            data.user.collectionCallbacks.registerNoSpaceNotifyer.forEach(callback => callback(ERRORS.TOO_LOT_EMPLOYEES));
            throw new Error(ERRORS.TOO_LOT_EMPLOYEES);
          }

          const user = new User(name, lastName);
    
          data.user.users.push(user);
          data.admin.company.curSize += 1;
    
          data.user.collectionCallbacks.registerUserCreateCallback.forEach(callback => callback(user, data.admin.company.curSize));
    
          return user;
        }

        this.deleteUser = function(id) {
          this.#checkAdmin();
          if (this.id == id) {
            throw new Error(ERRORS.CANT_DELETE_ADMIN);
          }

          data.user.users = data.user.users.filter(user => user.id !== id);
          data.user.collectionCallbacks.registerUserUpdateCallback.forEach(callback => callback(id, data.admin.company.curSize));
          data.admin.company.curSize -= 1;
      }
    }
  }

  get isAdmin() {
    return this.#isAdmin;
  }

  get name() {
    return this.#name;
  }

  set name (name) {
    this.#name = name;
    data.user.collectionCallbacks.registerUserUpdateCallback.forEach(callback => callback(this, data.admin.company.curSize));
  }

  get lastName() {
    return this.#lastName;
  }

  set lastName (lastName) {
    this.#lastName = lastName;
    data.user.collectionCallbacks.registerUserUpdateCallback.forEach(callback => callback(this, data.admin.company.curSize));
  }

  get role() {
    return this.#role;
  }

  set role(role) {
    this.#role = role;
    data.user.collectionCallbacks.registerUserUpdateCallback.forEach(callback => callback(this, data.admin.company.curSize));
  }

  #checkAdmin() {
    if (!this.#isAdmin) {
      throw new Error(ERRORS.INCORRECT_TOKEN);
    }
  }
}

	class Company {
    curSize = 0;

		constructor(name, maxSize) {
			this.name = name;
			this.maxSize = maxSize;
      this.curSize = 0;
      data.user = {
        collectionCallbacks: {
          registerUserCreateCallback: [],
          registerUserUpdateCallback: [],
          registerNoSpaceNotifyer: []
        },
        users: []
      };
		};

    static createSuperAdmin(company) {
      if (data.user.users.length >= 1) {
        throw new Error(ERRORS.ALREADY_HAVE_ADMIN);
      }
      const admin = new User('admin', 'admin', true);

      data.admin = {
        company,
        password: prompt(ENTER_PASSWORD),
        token: ADMIN_TOKEN
      }

      data.user.users.push(admin);
      data.admin.company.curSize += 1;

      return admin;
		}

    registerUserCreateCallback = (callback) => {
      data.user.collectionCallbacks.registerUserCreateCallback.push(callback);
    };

    registerUserUpdateCallback = (callback) => {
      data.user.collectionCallbacks.registerUserUpdateCallback.push(callback);
    };

    registerNoSpaceNotifyer = (callback) => {
      data.user.collectionCallbacks.registerNoSpaceNotifyer.push(callback);
    };

		getUser (id) {
      return data.user.find((item,)=>item.id === id);
		}
	}

	window.Company = Company;
})();


